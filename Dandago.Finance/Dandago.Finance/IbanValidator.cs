﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Finance
{
    public static class IbanValidator
    {
        #region IBAN Lengths

        private static Dictionary<string, int> ibanLengths = new Dictionary<string, int>()
        { // source: http://www.swift.com/dsp/resources/documents/IBAN_Registry.pdf
            {"AL", 28},
            {"AD", 24},
            {"AT", 20},
            {"AZ", 28},
            {"BH", 22},
            {"BE", 16},
            {"BA", 20},
            {"BR", 29},
            {"BG", 22},
            {"CR", 21},
            {"HR", 21},
            {"CY", 28},
            {"CZ", 24},
            {"DK", 18}, // Denmark
            {"FO", 18}, // Denmark
            {"GL", 18}, // Denmark
            {"DO", 28},
            {"EE", 20},
            {"FI", 18},
            {"FR", 27},
            {"GE", 22},
            {"DE", 22},
            {"GI", 23},
            {"GR", 27},
            {"GT", 28},
            {"HU", 28},
            {"IS", 26},
            {"IE", 22},
            {"IL", 23},
            {"IT", 27},
            {"JO", 30},
            {"KZ", 20},
            {"XK", 20},
            {"KW", 30},
            {"LV", 21},
            {"LB", 28},
            {"LI", 21},
            {"LT", 20},
            {"LU", 20},
            {"MK", 19},
            {"MT", 31},
            {"MR", 27},
            {"MU", 30},
            {"MD", 24},
            {"MC", 27},
            {"ME", 22},
            {"NL", 18},
            {"NO", 15},
            {"PK", 24},
            {"PS", 29},
            {"PL", 28},
            {"PT", 25},
            {"RO", 24},
            {"QA", 29},
            {"LC", 32},
            {"SM", 27},
            {"SA", 24},
            {"RS", 22},
            {"SK", 24},
            {"SI", 19},
            {"ES", 24},
            {"SE", 24},
            {"CH", 21},
            {"TL", 23},
            {"TN", 24},
            {"TR", 26},
            {"AE", 23},
            {"GB", 22},
            {"VG", 24},
        };

        #endregion IBAN Lengths

        // IBAN:
        // XX YY ZZZ...
        // ^  ^  ^
        // |  |  +-- Country code
        // |  +----- Check digits
        // +-------- BBAN

        public static bool IsValidIban(string iban)
        { // based on: https://en.wikipedia.org/wiki/International_Bank_Account_Number#Validating_the_IBAN
            // null/empty check

            if (string.IsNullOrWhiteSpace(iban))
                return false;

            // length check

            if (iban.Length > 34)
                return false;

            // uppercase (for ease of use)

            iban = iban.ToUpperInvariant();

            // country-specific length check

            var countryCode = iban.Substring(0, 2);

            if (ibanLengths.ContainsKey(countryCode))
            {
                var expectedIbanLength = ibanLengths[countryCode];

                if (expectedIbanLength != iban.Length)
                    return false;
            }
            else
                return false;

            // check digits

            string rearrangedIban = iban.Substring(4) + iban.Substring(0, 4); // move first 4 chars to end
            int[] integerIban = rearrangedIban.Select(c => IbanCharToInt(c)).ToArray();

            StringBuilder integerIbanSb = new StringBuilder();
            foreach (var i in integerIban)
                integerIbanSb.Append(i.ToString());

            BigInteger integerIbanBigInteger = BigInteger.Parse(integerIbanSb.ToString());
            BigInteger remainder = integerIbanBigInteger % 97;

            bool validCheckDigits = remainder == 1L;

            return validCheckDigits;
        }

        private static int IbanCharToInt(char c)
        {
            // numbers remain as is; A = 10, B = 11, ..., Z = 35

            if (c >= '0' && c <= '9')
                return c - 48;
            else if (c >= 'A' && c <= 'Z')
                return c - 55;
            else
                return -1;
        }
    }
}
