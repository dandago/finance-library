﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Finance
{
    public static class LuhnAlgorithm
    {
        public static bool IsValid(string input)
        {
            int total = 0;
            bool alternateCharacter = false; // we double every second character

            string reverseInput = new string(input.Reverse().ToArray());

            foreach (char c in reverseInput)
            {
                if (c >= '0' && c <= '9')
                {
                    int cInt = c - 48;

                    if (alternateCharacter)
                    {
                        cInt *= 2;

                        if (cInt > 9)
                            cInt -= 9;
                    }

                    total += cInt;
                    alternateCharacter = !alternateCharacter;
                }
                else
                    throw new ArgumentException("Input must have only numeric characters");
            }

            int modulus = total % 10;
            return modulus == 0;
        }
    }
}
