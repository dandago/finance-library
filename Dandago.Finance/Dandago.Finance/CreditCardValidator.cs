﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Finance
{
    public static class CreditCardValidator
    {
        public static bool IsValidDinersClub(string cardNumber)
        {
            if (string.IsNullOrWhiteSpace(cardNumber))
                return false;

            bool validStructure = IsValidDinersClubStructure(cardNumber);

            bool validCheckDigits = LuhnAlgorithm.IsValid(cardNumber);

            return validStructure && validCheckDigits;
        }

        public static bool IsValidAmericanExpress(string cardNumber)
        {
            if (string.IsNullOrWhiteSpace(cardNumber))
                return false;

            bool validStructure = IsValidAmericanExpressStructure(cardNumber);

            bool validCheckDigits = LuhnAlgorithm.IsValid(cardNumber);

            return validStructure && validCheckDigits;
        }

        public static bool IsValidVisa(string cardNumber)
        {
            if (string.IsNullOrWhiteSpace(cardNumber))
                return false;

            bool validStructure = IsValidVisaStructure(cardNumber);

            bool validCheckDigits = LuhnAlgorithm.IsValid(cardNumber);

            return validStructure && validCheckDigits;
        }

        public static bool IsValidMasterCard(string cardNumber)
        {
            if (string.IsNullOrWhiteSpace(cardNumber))
                return false;

            bool validStructure = IsValidMasterCardStructure(cardNumber);

            bool validCheckDigits = LuhnAlgorithm.IsValid(cardNumber);

            return validStructure && validCheckDigits;
        }

        public static bool IsValidDiscover(string cardNumber)
        {
            if (string.IsNullOrWhiteSpace(cardNumber))
                return false;

            bool validStructure = IsValidDiscoverStructure(cardNumber);

            bool validCheckDigits = LuhnAlgorithm.IsValid(cardNumber);

            return validStructure && validCheckDigits;
        }

        public static bool IsValidCreditCard(string cardNumber)
        {
            bool validStructure = IsValidDinersClubStructure(cardNumber)
                || IsValidAmericanExpressStructure(cardNumber)
                || IsValidVisaStructure(cardNumber)
                || IsValidMasterCardStructure(cardNumber)
                || IsValidDiscoverStructure(cardNumber);

            bool validCheckDigits = LuhnAlgorithm.IsValid(cardNumber);

            return validStructure && validCheckDigits;
        }

        private static bool IsValidDinersClubStructure(string cardNumber)
        {
            bool validStructure = cardNumber.Length == 14
                && (cardNumber.StartsWith("300")
                    || cardNumber.StartsWith("301")
                    || cardNumber.StartsWith("301")
                    || cardNumber.StartsWith("302")
                    || cardNumber.StartsWith("303")
                    || cardNumber.StartsWith("304")
                    || cardNumber.StartsWith("305")
                    || cardNumber.StartsWith("36")
                    || cardNumber.StartsWith("38")
                );

            return validStructure;
        }

        private static bool IsValidAmericanExpressStructure(string cardNumber)
        {
            bool validStructure = cardNumber.Length == 15
                && (cardNumber.StartsWith("34") || cardNumber.StartsWith("37"));

            return validStructure;
        }

        private static bool IsValidVisaStructure(string cardNumber)
        {
            bool validStructure = ((cardNumber.Length == 16 || cardNumber.Length == 13)
                && cardNumber.StartsWith("4"));

            return validStructure;
        }

        private static bool IsValidMasterCardStructure(string cardNumber)
        {
            bool validStructure = cardNumber.Length == 16
                && (cardNumber.StartsWith("51")
                    || cardNumber.StartsWith("52")
                    || cardNumber.StartsWith("53")
                    || cardNumber.StartsWith("54")
                    || cardNumber.StartsWith("55")
                );

            return validStructure;
        }

        private static bool IsValidDiscoverStructure(string cardNumber)
        {
            bool validStructure = cardNumber.Length == 16
                && (cardNumber.StartsWith("6011") || cardNumber.StartsWith("65"));

            return validStructure;
        }
    }
}
