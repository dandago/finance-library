﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xunit;

namespace Dandago.Finance.Tests
{
    public class CreditCardValidatorTests
    {
        // sample card numbers from: https://www.paypalobjects.com/en_US/vhelp/paypalmanager_help/credit_card_numbers.htm

        [Fact]
        public void IsValidDinersClub_ValidDinersClub_Valid()
        {
            string[] cardNumbers = new[] { "30569309025904", "38520000023237"};

            foreach(var cardNumber in cardNumbers)
            {
                bool valid = CreditCardValidator.IsValidDinersClub(cardNumber);
                Assert.True(valid);
            }
        }

        [Fact]
        public void IsValidDinersClub_InvalidDinersClub_Invalid()
        {
            string[] cardNumbers = new[] { "99569309035904" };

            foreach (var cardNumber in cardNumbers)
            {
                bool valid = CreditCardValidator.IsValidDinersClub(cardNumber);
                Assert.False(valid);
            }
        }

        [Fact]
        public void IsValidAmericanExpress_ValidAmericanExpress_Valid()
        {
            string[] cardNumbers = new[] { "378282246310005", "371449635398431", "378734493671000" };

            foreach (var cardNumber in cardNumbers)
            {
                bool valid = CreditCardValidator.IsValidAmericanExpress(cardNumber);
                Assert.True(valid);
            }
        }

        [Fact]
        public void IsValidAmericanExpress_InvalidAmericanExpress_Invalid()
        {
            string[] cardNumbers = new[] { "99569309035904" };

            foreach (var cardNumber in cardNumbers)
            {
                bool valid = CreditCardValidator.IsValidAmericanExpress(cardNumber);
                Assert.False(valid);
            }
        }

        [Fact]
        public void IsValidVisa_ValidVisa_Valid()
        {
            string[] cardNumbers = new[] { "4111111111111111", "4012888888881881", "4222222222222" };

            foreach (var cardNumber in cardNumbers)
            {
                bool valid = CreditCardValidator.IsValidVisa(cardNumber);
                Assert.True(valid);
            }
        }

        [Fact]
        public void IsValidVisa_InvalidVisa_Invalid()
        {
            string[] cardNumbers = new[] { "99569309035904" };

            foreach (var cardNumber in cardNumbers)
            {
                bool valid = CreditCardValidator.IsValidVisa(cardNumber);
                Assert.False(valid);
            }
        }

        [Fact]
        public void IsValidMasterCard_ValidMasterCard_Valid()
        {
            string[] cardNumbers = new[] { "5555555555554444", "5105105105105100" };

            foreach (var cardNumber in cardNumbers)
            {
                bool valid = CreditCardValidator.IsValidMasterCard(cardNumber);
                Assert.True(valid);
            }
        }

        [Fact]
        public void IsValidMasterCard_InvalidMasterCard_Invalid()
        {
            string[] cardNumbers = new[] { "99569309035904" };

            foreach (var cardNumber in cardNumbers)
            {
                bool valid = CreditCardValidator.IsValidMasterCard(cardNumber);
                Assert.False(valid);
            }
        }

        [Fact]
        public void IsValidDiscover_ValidDiscover_Valid()
        {
            string[] cardNumbers = new[] { "6011111111111117", "6011000990139424" };

            foreach (var cardNumber in cardNumbers)
            {
                bool valid = CreditCardValidator.IsValidDiscover(cardNumber);
                Assert.True(valid);
            }
        }

        [Fact]
        public void IsValidDiscover_InvalidDiscover_Invalid()
        {
            string[] cardNumbers = new[] { "99569309035904" };

            foreach (var cardNumber in cardNumbers)
            {
                bool valid = CreditCardValidator.IsValidDiscover(cardNumber);
                Assert.False(valid);
            }
        }

        [Fact]
        public void IsValidCreditCard_AllSupportedCards_Valid()
        {
            string[] cardNumbers = new[]
            {
                "30569309025904", "38520000023237",
                "378282246310005", "371449635398431", "378734493671000",
                "4111111111111111", "4012888888881881", "4222222222222",
                "5555555555554444", "5105105105105100",
                "6011111111111117", "6011000990139424"
            };

            foreach (var cardNumber in cardNumbers)
            {
                bool valid = CreditCardValidator.IsValidCreditCard(cardNumber);
                Assert.True(valid);
            }
        }

        [Fact]
        public void IsValidCreditCard_InvalidCreditCard_Invalid()
        {
            string[] cardNumbers = new[] { "99569309035904" };

            foreach (var cardNumber in cardNumbers)
            {
                bool valid = CreditCardValidator.IsValidCreditCard(cardNumber);
                Assert.False(valid);
            }
        }
    }
}
